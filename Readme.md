There should be a description of Fortran API


It is implemented in Fortran 2003(2008,2015) object-oriented model.

MTSF file API object-model description
--------------------------------------




Peculiarities
-------------

All strings are stored as C-Strings, i.e. null terminated strings of char type.
To use string values in Fortran one should use special functions to convert 
C-Strings to Fortran strings.


